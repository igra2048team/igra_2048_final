#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h> 
#include "logics.h"


//stampanje table
void stampajTablu(int **a, int velicina) {
	for (int i = 0; i < velicina; i++) {
		for (int j = 0; j < velicina; j++) {
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}
}

//jednom ubaceno u program da napravi potrebne datoteke
void napraviBinarnu(int velicina) {
	rang niz[10];
	if (velicina == 4) {
		FILE *ulaz = fopen("rang4x4.txt", "w");
		for (int i = 0; i < 10; i++) {
			niz[i].brojPoena = 0;
			strcpy(niz[i].ime, "/");
			endekripcija(&niz[i].brojPoena, &niz[i].ime);
			fprintf(ulaz, "%d %s\n", niz[i].brojPoena, niz[i].ime);
		}
		fclose(ulaz);
	}
	else
	{
		FILE *ulaz = fopen("rang5x5.txt", "w");
		for (int i = 0; i < 10; i++) {
			niz[i].brojPoena = 0;
			strcpy(niz[i].ime, "/");
			endekripcija(&niz[i].brojPoena, &niz[i].ime);
			fprintf(ulaz, "%d %s\n", niz[i].brojPoena, niz[i].ime);
		}
		fclose(ulaz);
	}
}


//XOR enkripcija i dekripcija
void endekripcija(int *brojPoena, char *ime) {
	char *text = malloc(sizeof(char) * 100);
	*brojPoena = 5 ^ *brojPoena;
	for (int i = 0; ime[i]; i++) {
		ime[i] = ime[i] ^ 5;
	}
}

//dohvata podatke iz fajla i stavlja ih u niz od 10, vraca pokazivac na prvi element
rang *dohvatiListu(int velicina) {
	char imeDat[15];
	if (velicina == 4) strcpy(imeDat, "rang4x4.txt");
	else if (velicina == 5)strcpy(imeDat, "rang5x5.txt");
	FILE *fajl = fopen(imeDat, "r");
	rang *lista = malloc(sizeof(rang) * 10);
	for (int i = 0; i < 10; i++) {
		fscanf(fajl, "%x %s", &lista[i].brojPoena, &lista[i].ime);
		endekripcija(&lista[i].brojPoena, &lista[i].ime);
	}
	fclose(fajl);
	return lista;
}


//dohvata podatke iz fajla, upisuje nove podatke ako ima promene
void upisiUListu(int velicina, int brojPoena, char ime[30]) {
	char imeDat[15];
	if (velicina == 4) strcpy(imeDat, "rang4x4.txt");
	else if (velicina == 5)strcpy(imeDat, "rang5x5.txt");
	FILE *fajl = fopen(imeDat, "r");
	rang *lista = malloc(sizeof(rang) * 10);
	for (int i = 0; i < 10; i++) {
		fscanf(fajl, "%x %s", &lista[i].brojPoena, &lista[i].ime);
		endekripcija(&lista[i].brojPoena, &lista[i].ime);
	}
	fclose(fajl);
	fajl = fopen(imeDat, "w");
	rang upis;
	upis.brojPoena = brojPoena, strcpy(upis.ime, ime);
	int flag = 0;
	for (int i = 0; i < 10; i++) {
		if (lista[i].brojPoena < brojPoena && !flag) {
			rang x = upis;
			upis = lista[i];
			lista[i] = x;
			flag = 1;
		}
		else if (flag) {
			rang x = upis;
			upis = lista[i];
			lista[i] = x;
		}
		endekripcija(&lista[i].brojPoena, &lista[i].ime);
		fprintf(fajl, "%x %s\n", lista[i].brojPoena, lista[i].ime);
		endekripcija(&lista[i].brojPoena, &lista[i].ime);
	}
	fclose(fajl);
}


//brise listu
void brisiListu(rang *lista) {
	if(lista) free(lista);
}

//inicijalizacija table
int **init(int velicina) {
	int **tabla;
	tabla = malloc(velicina * sizeof(int*));
	PROVERI_ALLOC(tabla);
	for (int i = 0; i < velicina; i++) {
		tabla[i] = calloc(velicina, sizeof(int));
		PROVERI_ALLOC(tabla[i]);
	}
	int x, y;
	srand(time(NULL));
	x = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
	y = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
	tabla[x][y] = 2;
	do {
		x = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
		y = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
	} while (tabla[x][y] != 0);
	tabla[x][y] = 2;
	return tabla;
}

/*Tabla i skor se prenose po referenci, vraca 1 ako ima pomeranja*/
int pomeranje(int **tabla, int velicina, int pravac, int *skor) {
	int promena = 0;
	switch (pravac) {
		/*Po svakoj koloni krece od dna i nalazi prvu praznu poziciju.
		Ako je polje ispod prve prazne pozicije iste vrednosti kao i posmatrana plocica
		sabira sa njom, a ako nije onda je stavlja na mesto prve prazne pozicije.
		Ako se saberu, flag "sabrano" se postavlja na jedan da ne bi doslo do lancanog sabiranja.*/
	case 1:
		//levo
		for (int i = 0; i < velicina; i++) {
			int k = 0;
			int sabrano = 0;
			for (int j = 0; j < velicina; j++) {
				if (!tabla[i][j]) continue;
				while (tabla[i][k++] && k < velicina);
				k--;
				if (k < j) {
					if (k > 0 && tabla[i][k - 1] == tabla[i][j] && !sabrano) {
						tabla[i][k - 1] *= 2;
						*skor += tabla[i][k - 1];
						sabrano = 1;
					}
					else {
						tabla[i][k] = tabla[i][j];
						sabrano = 0;
					}
					tabla[i][j] = 0;
					promena = 1;
				}
				else {
					if (j && tabla[i][j - 1] == tabla[i][j]) {
						tabla[i][j - 1] *= 2;
						tabla[i][j] = 0;
						k = j;
						sabrano = 1;
						promena = 1;
						*skor += tabla[i][j - 1];
					}
				}
			}
		}
		break;
	case 2:
		//gore
		for (int i = 0; i < velicina; i++) {
			int k = 0;
			int sabrano = 0;
			for (int j = 0; j < velicina; j++) {
				if (!tabla[j][i]) continue;
				while (tabla[k++][i] && k < velicina);
				k--;
				if (k < j) {
					if (k > 0 && tabla[k - 1][i] == tabla[j][i] && !sabrano) {
						tabla[k - 1][i] *= 2;
						sabrano = 1;
						*skor += tabla[k - 1][i];
					}
					else {
						tabla[k][i] = tabla[j][i];
						sabrano = 0;
					}
					tabla[j][i] = 0;
					promena = 1;
				}
				else {
					if (j && tabla[j - 1][i] == tabla[j][i]) {
						tabla[j - 1][i] *= 2;
						tabla[j][i] = 0;
						sabrano = 1;
						k = j;
						promena = 1;
						*skor += tabla[j - 1][i];
					}
				}
			}
		}
		break;
	case 3:
		//desno
		for (int i = 0; i < velicina; i++) {
			int k = velicina - 1;
			int sabrano = 0;
			for (int j = velicina - 1; j >= 0; j--) {
				if (!tabla[i][j]) continue;
				while (tabla[i][k--] && k >= 0);
				k++;
				if (k > j) {
					if (k < (velicina - 1) && tabla[i][k + 1] == tabla[i][j] && !sabrano) {
						tabla[i][k + 1] *= 2;
						sabrano = 1;
						*skor += tabla[i][k + 1];
					}
					else {
						tabla[i][k] = tabla[i][j];
						sabrano = 0;
					}
					tabla[i][j] = 0;
					promena = 1;
				}
				else {
					if (j != (velicina - 1) && tabla[i][j + 1] == tabla[i][j]) {
						tabla[i][j + 1] *= 2;
						tabla[i][j] = 0;
						k = j;
						promena = 1;
						sabrano = 1;
						*skor += tabla[i][j + 1];
					}
				}
			}
		}
		break;
	case 4:
		//dole
		for (int i = 0; i < velicina; i++) {
			int k = velicina - 1;
			int sabrano = 0;
			for (int j = velicina - 1; j >= 0; j--) {
				if (!tabla[j][i]) continue;
				while (tabla[k--][i] && k >= 0);
				k++;
				if (k > j) {
					if (k < (velicina - 1) && tabla[k + 1][i] == tabla[j][i] && !sabrano) {
						tabla[k + 1][i] *= 2;
						sabrano = 1;
						*skor += tabla[k + 1][i];
					}
					else {
						tabla[k][i] = tabla[j][i];
						sabrano = 0;
					}
					tabla[j][i] = 0;
					promena = 1;
				}
				else {
					if (j != (velicina - 1) && tabla[j + 1][i] == tabla[j][i]) {
						tabla[j + 1][i] *= 2;
						tabla[j][i] = 0;
						k = j;
						promena = 1;
						sabrano = 1;
						skor += tabla[j + 1][i];
					}
				}
			}
		}
		break;
	}
	/*Ako ima promene vraca 1, u suprotnom 0*/
	return promena;
}

//igra jedan potez
void potez(int **tabla, int velicina, int pravac, int *skor) {
	if (!pomeranje(tabla, velicina, pravac, skor)) return 1;
	else {
		int x, y;
		srand(time(NULL));
		do {
			x = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
			y = (int)(rand() / ((double)RAND_MAX + 1.0)*(velicina));
		} while (tabla[x][y] != 0);
		int broj = (rand() / (double)(RAND_MAX)) > 0.7 ? 4 : 2;
		tabla[x][y] = broj;
		return 0;
	}
}

int pomeranjeBezskora(int **tabla, int velicina, int pravac) {
	int promena = 0;
	switch (pravac) {
		/*Po svakoj koloni krece od dna i nalazi prvu praznu poziciju.
		Ako je polje ispod prve prazne pozicije iste vrednosti kao i posmatrana plocica
		sabira sa njom, a ako nije onda je stavlja na mesto prve prazne pozicije.
		Ako se saberu, flag "sabrano" se postavlja na jedan da ne bi doslo do lancanog sabiranja.*/
	case 1:
		//levo
		for (int i = 0; i < velicina; i++) {
			int k = 0;
			int sabrano = 0;
			for (int j = 0; j < velicina; j++) {
				if (!tabla[i][j]) continue;
				while (tabla[i][k++] && k < velicina);
				k--;
				if (k < j) {
					if (k > 0 && tabla[i][k - 1] == tabla[i][j] && !sabrano) {
						tabla[i][k - 1] *= 2;
						sabrano = 1;
					}
					else {
						tabla[i][k] = tabla[i][j];
						sabrano = 0;
					}
					tabla[i][j] = 0;
					promena = 1;
				}
				else {
					if (j && tabla[i][j - 1] == tabla[i][j]) {
						tabla[i][j - 1] *= 2;
						tabla[i][j] = 0;
						k = j;
						sabrano = 1;
						promena = 1;
					}
				}
			}
		}
		break;
	case 2:
		//gore
		for (int i = 0; i < velicina; i++) {
			int k = 0;
			int sabrano = 0;
			for (int j = 0; j < velicina; j++) {
				if (!tabla[j][i]) continue;
				while (tabla[k++][i] && k < velicina);
				k--;
				if (k < j) {
					if (k > 0 && tabla[k - 1][i] == tabla[j][i] && !sabrano) {
						tabla[k - 1][i] *= 2;
						sabrano = 1;
					}
					else {
						tabla[k][i] = tabla[j][i];
						sabrano = 0;
					}
					tabla[j][i] = 0;
					promena = 1;
				}
				else {
					if (j && tabla[j - 1][i] == tabla[j][i]) {
						tabla[j - 1][i] *= 2;
						tabla[j][i] = 0;
						sabrano = 1;
						k = j;
						promena = 1;
					}
				}
			}
		}
		break;
	case 3:
		//desno
		for (int i = 0; i < velicina; i++) {
			int k = velicina - 1;
			int sabrano = 0;
			for (int j = velicina - 1; j >= 0; j--) {
				if (!tabla[i][j]) continue;
				while (tabla[i][k--] && k >= 0);
				k++;
				if (k > j) {
					if (k < (velicina - 1) && tabla[i][k + 1] == tabla[i][j] && !sabrano) {
						tabla[i][k + 1] *= 2;
						sabrano = 1;
					}
					else {
						tabla[i][k] = tabla[i][j];
						sabrano = 0;
					}
					tabla[i][j] = 0;
					promena = 1;
				}
				else {
					if (j != (velicina - 1) && tabla[i][j + 1] == tabla[i][j]) {
						tabla[i][j + 1] *= 2;
						tabla[i][j] = 0;
						k = j;
						promena = 1;
						sabrano = 1;
					}
				}
			}
		}
		break;
	case 4:
		//dole
		for (int i = 0; i < velicina; i++) {
			int k = velicina - 1;
			int sabrano = 0;
			for (int j = velicina - 1; j >= 0; j--) {
				if (!tabla[j][i]) continue;
				while (tabla[k--][i] && k >= 0);
				k++;
				if (k > j) {
					if (k < (velicina - 1) && tabla[k + 1][i] == tabla[j][i] && !sabrano) {
						tabla[k + 1][i] *= 2;
						sabrano = 1;
					}
					else {
						tabla[k][i] = tabla[j][i];
						sabrano = 0;
					}
					tabla[j][i] = 0;
					promena = 1;
				}
				else {
					if (j != (velicina - 1) && tabla[j + 1][i] == tabla[j][i]) {
						tabla[j + 1][i] *= 2;
						tabla[j][i] = 0;
						k = j;
						promena = 1;
						sabrano = 1;
					}
				}
			}
		}
		break;
	}
	/*Ako ima promene vraca 1, u suprotnom 0*/
	return promena;
}