#pragma once
#include <SDL.h>


void score(SDL_Window*, SDL_Renderer*);
void opcije(int*, int*, SDL_Window*, SDL_Renderer*);
int play(int, int*, SDL_Window*, SDL_Renderer*);
void autori(SDL_Window*, SDL_Renderer*, int);
void info(SDL_Window*, SDL_Renderer*);
int** LoadImage(SDL_Window*, SDL_Renderer*, int**, int,int,int,int*,int,char*,int,char*);
char unesiIme(char);