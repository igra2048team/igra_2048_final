#include <stdio.h>
#include <stdlib.h>
#include "logics.h"
#include "hintAlgo.h"

int validnaPoz(int **tabla, int velicina) {
	for (int i = 0; i < velicina; i++) {
		for (int j = 0; j < velicina - 1; j++) {
			if (!tabla[i][j] || !tabla[i][j + 1] || tabla[i][j] == tabla[i][j + 1])
				return 1;
			if (!tabla[j][i] || !tabla[j + 1][i] || tabla[j][i] == tabla[j + 1][i])
				return 1;
		}
	}
	return 0;
}

float evaluacija(int **tabla, int velicina) {//O(n^2)
	int suma = 0, prazna = 0, ugao, maxUgao = 0;
	float sideScore, maxScore = 0.;//za drugu
	if (!validnaPoz(tabla, velicina))
		return 0.;

	for (int i = 0; i < velicina; i++) {//prva heuristika - broj praznih nakon poteza
		for (int j = 0; j < velicina; j++) {
			if (!tabla[i][j])
				prazna++;
			else
				suma += tabla[i][j];
		}
	}

	//druga heuristika - broj velikih po uglovima
	//1-2
	ugao = 0;
	for (int i = 0; i < velicina; i++) {
		ugao += tabla[i][0];
	}
	for (int i = 1; i < velicina; i++) {
		ugao += tabla[0][i];
	}
	sideScore = ((float)ugao / suma);
	if (maxScore < sideScore)
		maxScore = sideScore;
	//2-3
	ugao = 0;
	for (int i = 0; i < velicina; i++) {
		ugao += tabla[0][i];
	}
	for (int i = 1; i < velicina; i++) {
		ugao += tabla[i][velicina - 1];
	}
	sideScore = ((float)ugao / suma);
	if (maxScore < sideScore)
		maxScore = sideScore;
	//3-4
	ugao = 0;
	for (int i = 0; i < velicina; i++) {
		ugao += tabla[i][velicina - 1];
	}
	for (int i = 0; i < velicina - 1; i++) {
		ugao += tabla[velicina - 1][i];
	}
	sideScore = ((float)ugao / suma);
	if (maxScore < sideScore)
		maxScore = sideScore;
	//4-1
	ugao = 0;
	for (int i = 0; i < velicina; i++) {
		ugao += tabla[i][0];
	}
	for (int i = 1; i < velicina; i++) {
		ugao += tabla[velicina - 1][i];
	}
	sideScore = ((float)ugao / suma);
	if (maxScore < sideScore)
		maxScore = sideScore;


	//1-2
	ugao = tabla[0][0];
	ugao += (tabla[0][1] + tabla[1][0]) / 2;
	ugao += (tabla[0][2] + tabla[2][0]) / 2;
	if (ugao > maxUgao)
		maxUgao = ugao;

	//2-3
	ugao = tabla[0][velicina - 1];
	ugao += (tabla[0][velicina - 2] + tabla[1][velicina - 1]) / 2;
	ugao += (tabla[0][velicina - 3] + tabla[2][velicina - 1]) / 4;
	if (ugao > maxUgao)
		maxUgao = ugao;

	//3-4
	ugao = tabla[velicina - 1][velicina - 1];
	ugao += (tabla[velicina - 2][velicina - 1] + tabla[velicina - 1][velicina - 2]) / 2;
	ugao += (tabla[velicina - 3][velicina - 1] + tabla[velicina - 1][velicina - 3]) / 4;
	if (ugao > maxUgao)
		maxUgao = ugao;

	//4-1
	ugao = tabla[velicina - 1][0];
	ugao += (tabla[velicina - 2][0] + tabla[velicina - 1][1]) / 2;
	ugao += (tabla[velicina - 3][0] + tabla[velicina - 1][2]) / 4;
	if (ugao > maxUgao)
		maxUgao = ugao;

	//return 0.27*prazna + 0.40*maxScore;
	return 0.4*(prazna / (velicina*velicina)) + maxUgao / suma + 0.4*maxScore;
}


int **initTabla(int velicina, int **tablaOriginal) {
	int **tabla;
	tabla = malloc(velicina * sizeof(int*));
	PROVERI_ALLOC(tabla);
	for (int i = 0; i < velicina; i++) {
		tabla[i] = calloc(velicina, sizeof(int));
		PROVERI_ALLOC(tabla[i]);
		for (int j = 0; j < velicina; j++)
			tabla[i][j] = tablaOriginal[i][j];
	}
	return tabla;
}


void kopirajTablu(int **tabla, int **tablaOriginal, int velicina) {
	for (int i = 0; i < velicina; i++) {
		for (int j = 0; j < velicina; j++)
			tabla[i][j] = tablaOriginal[i][j];
	}
}


//pozicija mora biti validna!
int pozoviProveru(int **tabla, int velicina, int limit) {
	float maxScore = 0., scr = 0., scr2, scr4;
	int pravac = 0, count;
	int **kopijaTable;

	kopijaTable = initTabla(velicina, tabla);

	//proveriti malloc-e!
	//1-levo
	if (pomeranjeBezskora(kopijaTable, velicina, 1)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}
			}
		scr = (0.8*scr2 + 0.2*scr4) / count;
		if (scr > maxScore) {
			maxScore = scr;
			pravac = 1;
		}
	}

	kopirajTablu(kopijaTable, tabla, velicina);

	//2-gore
	if (pomeranjeBezskora(kopijaTable, velicina, 2)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}
			}
		scr = (0.8*scr2 + 0.2*scr4) / count;//proveri divide by zero!!!
		if (scr > maxScore) {
			maxScore = scr;
			pravac = 2;
		}
	}

	kopirajTablu(kopijaTable, tabla, velicina);

	//3-desno
	if (pomeranjeBezskora(kopijaTable, velicina, 3)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}

			}
		scr = (0.8*scr2 + 0.2*scr4) / count;
		if (scr > maxScore) {
			maxScore = scr;
			pravac = 3;
		}
	}

	kopirajTablu(kopijaTable, tabla, velicina);

	//4-dole
	if (pomeranjeBezskora(kopijaTable, velicina, 4)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}
			}
		scr = (0.8*scr2 + 0.2*scr4) / count;
		if (scr > maxScore) {
			maxScore = scr;
			pravac = 4;
		}
	}

	for (int i = 0; i < velicina; i++) {
		free(kopijaTable[i]);
	} free(kopijaTable);

	return pravac;
}

float proveraR(int **tabla, int velicina, int dubina, int limit) {
	int count;
	float scr = 0., scr2, scr4, cEval;
	int **kopijaTable;

	//debug
	//printf("\DUBINA proveraR je: %d LIMIT je: %d\n", dubina, limit);
	//kraj rekurzije, dostignuta dubina
	if (limit <= dubina) {
		return evaluacija(tabla, velicina);
	}


	kopijaTable = initTabla(velicina, tabla);

	cEval = evaluacija(kopijaTable, velicina);
	//proveriti malloc-e!
	//1-levo
	if (pomeranjeBezskora(kopijaTable, velicina, 1)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, dubina + 1, limit);//izbacen smer pretpostavljeni
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}

			}
		scr += (0.8*scr2 + 0.2*scr4) / count;//divide by zero!!!
	}

	kopirajTablu(kopijaTable, tabla, velicina);
	//2-gore
	if (pomeranjeBezskora(kopijaTable, velicina, 2)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}

			}
		scr += (0.8*scr2 + 0.2*scr4) / count;
	}

	kopirajTablu(kopijaTable, tabla, velicina);
	//3-desno
	if (pomeranjeBezskora(kopijaTable, velicina, 3)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}

			}
		scr += (0.8*scr2 + 0.2*scr4) / count;
	}

	kopirajTablu(kopijaTable, tabla, velicina);
	//4-dole
	if (pomeranjeBezskora(kopijaTable, velicina, 4)) {
		count = 0; scr2 = 0.; scr4 = 0.;
		for (int i = 0; i < velicina; i++)
			for (int j = 0; j < velicina; j++) {
				if (!kopijaTable[i][j]) {
					kopijaTable[i][j] = 2;
					scr2 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 4;
					scr4 += proveraR(kopijaTable, velicina, dubina + 1, limit);
					kopijaTable[i][j] = 0;
					count++;
				}

			}
		scr += (0.8*scr2 + 0.2*scr4) / count;
	}

	for (int i = 0; i < velicina; i++) {
		free(kopijaTable[i]);
	} free(kopijaTable);

	//float newSCR = scr / 4.0f;


	return (scr / 4.0) + cEval;

}