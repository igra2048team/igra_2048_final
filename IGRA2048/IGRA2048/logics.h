#pragma once

#define PROVERI_ALLOC(pok) if (!(pok)) {printf("Neuspesna alokacija!"); exit(-1); }

#ifndef rang

typedef struct r {
	char ime[30];
	int brojPoena;
}rang ;

#endif 


int skor;

void stampajTablu(int**, int);

int** init(int);

int pomeranje(int**, int, int, int*);

void potez(int**, int, int, int*);

int pomeranjeBezskora(int**, int , int );

void napraviBinarnu(int);

void brisiListu(rang *);

void upisiUListu(int, int, char [30]);

rang *dohvatiListu(int);

void endekripcija(int *, char *);