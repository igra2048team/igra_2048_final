#include "Source.h" 
#include <stdio.h>
#include "logics.h"
#include <SDL.h>
#include <SDL_ttf.h>
#define _WIN32_WINNT 0x0500
#include <windows.h>

/* vraca opciju kako bi se zapocela neka od opcija iz menija */
int meni(SDL_Renderer* renderTarget, SDL_Window *window, int opt, int *ex) {
	int x, y, i, j, sum = 50;
	const char *labels[6] = { "Igra", "Opcije","O igri","TOP 10","Autori", "Izlaz" };
	int selected[6] = { 0 };
	SDL_Event e;
	TTF_Font *font = TTF_OpenFont("STENCIL.TTF", 65);;
	SDL_Surface *menus[6] = { NULL };
	SDL_Color color[2] = { { 110,0,0 },{ 255,239,2 } }; 
	SDL_Rect pos[6];
	SDL_Texture *text[6] = { NULL };
	SDL_Surface* temp = NULL;
	SDL_Texture *texture = NULL;
	SDL_Rect pozadina;

	pozadina.x = 0;
	pozadina.y = 0;
	pozadina.w = 1300;
	pozadina.h = 1000;

	for (i = 0; i < 6; i++) {
		menus[i] = TTF_RenderText_Solid(font, labels[i], color[0]);
		text[i] = SDL_CreateTextureFromSurface(renderTarget, menus[i]);
	}


	for (i = 0; i < 6; i++) {
		if (i == 0) { pos[i].x = 500; pos[i].y = 195; }
		if (i == 1) { pos[i].x = 470; pos[i].y = 335; }
		if (i == 2) { pos[i].x = 475; pos[i].y = 475; }
		if (i == 3) { pos[i].x = 470; pos[i].y = 610; }
		if (i == 4) { pos[i].x = 460; pos[i].y = 745; }
		if (i == 5) { pos[i].x = 485; pos[i].y = 880; }
		SDL_QueryTexture(text[i], NULL, NULL, &pos[i].w, &pos[i].h);
	}
	texture = IMG_LoadTexture(renderTarget, "meni.bmp");


	while (*ex) {
		SDL_RenderClear(renderTarget);
		SDL_RenderCopy(renderTarget, texture, NULL, &(pozadina));
		for (int j = 0; j < 6; j++)
			SDL_RenderCopy(renderTarget, text[j], NULL, &pos[j]);
		SDL_RenderPresent(renderTarget);

		while (SDL_PollEvent(&e)) {
			switch (e.type)
			{
			case SDL_QUIT:
				for (j = 0; j < 6; j++) { if(menus[j]) SDL_FreeSurface(menus[j]); if(text[j]) SDL_DestroyTexture(text[j]); }
				if (temp)SDL_FreeSurface(temp);
				if (texture)SDL_DestroyTexture(texture);
				if (font) TTF_CloseFont(font);
				*ex = 0;
				break;

			case SDL_MOUSEBUTTONDOWN:
				x = e.button.x;
				y = e.button.y;
				for (i = 0; i < 6; i++)
					if ((x >= pos[i].x) && (x <= pos[i].x + pos[i].w) && (y >= pos[i].y) && (y <= pos[i].y + pos[i].h)) {
						if (i == 0) {
							for (j = 0; j < 6; j++) { if (menus[j])SDL_FreeSurface(menus[j]); if (text[j])SDL_DestroyTexture(text[j]); }
							if (temp)SDL_FreeSurface(temp);
							if (texture)SDL_DestroyTexture(texture);
							if (font)TTF_CloseFont(font);
							return 0;
						}
						if (i == 1) {
							for (j = 0; j < 6; j++) { if (menus[j]) SDL_FreeSurface(menus[j]); if(text[j])SDL_DestroyTexture(text[j]); }
							if (temp)SDL_FreeSurface(temp);
							if (temp)SDL_DestroyTexture(texture);
							if(font)TTF_CloseFont(font);
							return 1;
						}
						if (i == 2) {
							for (j = 0; j < 6; j++) { if(menus[j])SDL_FreeSurface(menus[j]); if(text[j]) SDL_DestroyTexture(text[j]); }
							if(temp)SDL_FreeSurface(temp); 
							if (texture)SDL_DestroyTexture(texture);
							if (font)TTF_CloseFont(font);
							return 2;
						}
						if (i == 3) {
							for (j = 0; j < 6; j++) { if (menus[j])SDL_FreeSurface(menus[j]); if (text[j])SDL_DestroyTexture(text[j]); }
							if (temp)SDL_FreeSurface(temp);
							if (texture)SDL_DestroyTexture(texture);
							if (font)TTF_CloseFont(font);
							return 3;
						}
						if (i == 4) {
							for (j = 0; j < 6; j++) { if (menus[j])SDL_FreeSurface(menus[j]); if (text[j])SDL_DestroyTexture(text[j]); }
							if (temp)SDL_FreeSurface(temp);
							if (texture)SDL_DestroyTexture(texture);
							if (font)TTF_CloseFont(font);
							return 4;
						}
						if (i == 5) {
							*ex = 0;
							for (j = 0; j < 6; j++) { if (menus[j])SDL_FreeSurface(menus[j]); if (text[j])SDL_DestroyTexture(text[j]); }
							if (temp)SDL_FreeSurface(temp);
							if (texture)SDL_DestroyTexture(texture);
							if (font)TTF_CloseFont(font);
						}
					
					}
				return 5;
			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE) {
					*ex = 0;
					for (j = 0; j < 6; j++) { if (menus[j])SDL_FreeSurface(menus[j]); if (text[j])SDL_DestroyTexture(text[j]); }
					if (temp)SDL_FreeSurface(temp);
					if (texture)SDL_DestroyTexture(texture);
					if (font)TTF_CloseFont(font);
				}
				return 5;

			}
		}
	}

}

int main(int argc, char *args[]) {
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);
	int opt=1, plocica = 2048,skor=0;
	int velicina = 4,ex=1;
	SDL_Window* window = NULL;
	SDL_Renderer *renderTarget = NULL;

	// INICIJALIZACIJA
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		printf("ERROR");
	if (TTF_Init() < 0) printf("ERROR");

	window = SDL_CreateWindow("IGRA 2048", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1300, 1000, SDL_WINDOW_SHOWN);
	//SDL_SetWindowFullscreen(window, 1);
	if (window == NULL) printf("Neuspjesno!");
	renderTarget = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);


	while (ex) {
		opt= meni(renderTarget, window, opt,&ex);
				switch (opt)
				{
				case 0:
					skor = play(skor, &velicina, window, renderTarget, plocica);    //IGRAJ 
					break;
				case 1:
					opcije(&velicina, &plocica, window, renderTarget);   //OPCIJE
					break;
				case 2:
					info(window, renderTarget);        //O IGRI(POMOC)
					break;
				case 3:
					score(window, renderTarget);       //NAJBOLJI SKOROVI
					break;
				case 4:
					autori(window, renderTarget, opt);     //AUTORI
					break;
			   }
				skor = 0;
		}
	

	//OSLOBADJANJE MEMORIJE
	if(renderTarget)SDL_DestroyRenderer(renderTarget);
	renderTarget = NULL;
	if(window)SDL_DestroyWindow(window);
	window = NULL;
	SDL_Quit();
	TTF_Quit();
	return 0;
}