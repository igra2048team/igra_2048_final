#include <stdio.h>
#include <SDL.h>
#include "SDL_main.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "Source.h"
#include "logics.h"
#include "hintAlgo.h"

/* ispisuje TOP10 skorova za tebele 5x5 i 4x4 */
void score(SDL_Window *window, SDL_Renderer* renderTarget) {
	int ex = 1, sum = 0, x, y, i, j,boo=1;
	char pom[10], pom1[10];
	const char *labels[3] = { "Velicina tabele","4x4","5x5" };
	SDL_Event e;
	SDL_Surface  *menus[3] = { NULL }, *redniBr[20] = { NULL }, *ime[20] = { NULL }, *brojP[20] = { NULL };
	SDL_Color color[2] = { { 110,0,0 },{ 170,0,0 } };
	SDL_Rect pos[3], poz1[20], poz2[20], poz3[20], poz;
	SDL_Texture *text[3] = { NULL }, *textRedniBr[20] = { NULL }, *textIme[20] = { NULL }, *textBrPoena[20] = { NULL };
	TTF_Font *font = TTF_OpenFont("ariblk.TTF", 65), *font1 = TTF_OpenFont("arialbd.TTF", 50);
	SDL_Texture *texture1 = IMG_LoadTexture(renderTarget, "povratak.bmp");
	rang *lista4 = NULL,*lista5=NULL;

	lista4 = dohvatiListu(4);
	lista5 = dohvatiListu(5);
	poz.x = 1120;   poz.w = 150;
	poz.y = 920;	poz.h = 90;
	

	for (i = 0; i < 3; i++) {
		if(i==0) menus[i] = TTF_RenderText_Solid(font, labels[i], color[0]);
		menus[i] = TTF_RenderText_Solid(font, labels[i], color[1]);
		text[i] = SDL_CreateTextureFromSurface(renderTarget, menus[i]);
	}

	for (i = 0; i < 3; i++) {
		if (i == 0) { pos[i].x = 356; pos[i].y = 5; }
		if (i == 1) { pos[i].x = 220; pos[i].y = 105; }
		if (i == 2) { pos[i].x = 900; pos[i].y = 105; }
		SDL_QueryTexture(text[i], NULL, NULL, &pos[i].w, &pos[i].h);
	}

	for (j = 0; j < 20; j++) {
		if ((j > 9) && (boo == 1)) { sum = 0; boo = 2; }
		if(j<=9) itoa(j + 1, pom, 10); else itoa(j + 1-10, pom, 10);
		strcat(pom, ".");
		if (j <= 9) {
			redniBr[j] = TTF_RenderText_Solid(font1, pom, color[1]);
			ime[j] = TTF_RenderText_Solid(font1, lista4[j].ime, color[1]);
			itoa(lista4[j].brojPoena, pom1, 10);
			brojP[j] = TTF_RenderText_Solid(font1, pom1, color[1]);
		}
		else
		{
			redniBr[j] = TTF_RenderText_Solid(font1, pom, color[1]);
			ime[j] = TTF_RenderText_Solid(font1, lista5[j-10].ime, color[1]);
			itoa(lista5[j-10].brojPoena, pom1, 10);
			brojP[j] = TTF_RenderText_Solid(font1, pom1, color[1]);
		}
		
		textRedniBr[j] = SDL_CreateTextureFromSurface(renderTarget, redniBr[j]);
		textIme[j] = SDL_CreateTextureFromSurface(renderTarget, ime[j]);
		textBrPoena[j] = SDL_CreateTextureFromSurface(renderTarget, brojP[j]);

		poz1[j].x = 10; poz1[j].y = 210 + sum; 	poz2[j].x = 100; poz2[j].y = 210 + sum;   	poz3[j].x = 475; poz3[j].y = 210 + sum;
		if (j>9) { poz1[j].x = 675; poz2[j].x = 765; poz3[j].x = 1120; }
		SDL_QueryTexture(textRedniBr[j], NULL, NULL, &poz1[j].w, &poz1[j].h);
		SDL_QueryTexture(textIme[j], NULL, NULL, &poz2[j].w, &poz2[j].h);
		SDL_QueryTexture(textBrPoena[j], NULL, NULL, &poz3[j].w, &poz3[j].h);
		sum = sum + 65;
		
	}
	SDL_SetRenderDrawColor(renderTarget, 232, 232, 232, 0xFF);

	while (ex) {
		SDL_RenderClear(renderTarget);
		SDL_RenderCopy(renderTarget, texture1, NULL, &(poz));
		for (int j = 0; j < 3; j++)
			SDL_RenderCopy(renderTarget, text[j], NULL, &pos[j]);
		for (j = 0; j < 20; j++) {
			SDL_RenderCopy(renderTarget, textRedniBr[j], NULL, &poz1[j]);
			SDL_RenderCopy(renderTarget, textIme[j], NULL, &poz2[j]);
			SDL_RenderCopy(renderTarget, textBrPoena[j], NULL, &poz3[j]);
		}
		SDL_RenderPresent(renderTarget);




		while (SDL_PollEvent(&e)) {
			switch (e.type)
			{
			case SDL_QUIT:
				ex = 0;
				break;

			case SDL_MOUSEBUTTONDOWN:
				x = e.button.x;
				y = e.button.y;
				if ((x >= poz.x) && (x <= poz.x + poz.w) && (y >= poz.y) && (y <= poz.y + poz.h)) {
							for (j = 0; j < 3; j++) {
								if (menus[j])SDL_FreeSurface(menus[j]);
								if (text[j])SDL_DestroyTexture(text[j]);
							}
							if (font)TTF_CloseFont(font);
							if (font1)TTF_CloseFont(font1);
							brisiListu(lista4);
							brisiListu(lista5);
							for (j = 0; j < 10; j++) {
								SDL_DestroyTexture(textBrPoena[j]);
								SDL_DestroyTexture(textIme[j]);
								SDL_DestroyTexture(textRedniBr[j]);
								SDL_FreeSurface(ime[j]);
								SDL_FreeSurface(brojP[j]);
								SDL_FreeSurface(redniBr[j]);
							}
							ex = 0;
							return 1;
						}
				break;
			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE) {
					ex = 0;
				}
			}
		}
}

	for (j = 0; j < 3; j++) {
		if (menus[j])SDL_FreeSurface(menus[j]);
		if (text[j])SDL_DestroyTexture(text[j]);
	}

	brisiListu(lista4);
	brisiListu(lista5);
	for (j = 0; j < 20; j++) {
		SDL_DestroyTexture(textBrPoena[j]);
		SDL_DestroyTexture(textIme[j]);
		SDL_DestroyTexture(textRedniBr[j]);
		SDL_FreeSurface(ime[j]);
		SDL_FreeSurface(brojP[j]);
		SDL_FreeSurface(redniBr[j]);
	}
	if (font)TTF_CloseFont(font);
	if (font1)TTF_CloseFont(font1);
	SDL_DestroyTexture(texture1);
	return 1;
}
/* ispisuje mogucnosti za odabir velicine tabele, kao i vrijednosti plocice, klikom misa na odredjenu stavku mijenja se velicina tj. vrijednost plocice;
    moguce je i izbrisati TOP10 skorova za onu tabelu koja je selektovana u opcijama velicina tabele    */
void opcije(int *velicina, int *plocica, SDL_Window* window, SDL_Renderer *renderTarget) {
	int ex = 1;
	int x, y, i, j, sum = 20;
	const char *labels[10] = { "Velicina tabele","4x4","5x5","Vrijednost plocice","256","512","1024","2048", "Brisanje TOP 10","Povratak na meni" };
	SDL_Event e;
	SDL_Surface *menus[10] = { NULL };
	SDL_Color color[10] = { { 110,0,0 },{ 170,0,0 } }; 
	SDL_Rect pos[10], izbor1, izbor2;
	SDL_Texture *text[10] = { NULL };
	SDL_Surface* temp = NULL;
	TTF_Font *font = TTF_OpenFont("STENCIL.TTF", 75);
	SDL_Texture *texture1 = IMG_LoadTexture(renderTarget, "true.bmp");
	SDL_Texture *texture2 = IMG_LoadTexture(renderTarget, "true.bmp");
	izbor1.x = 690;       izbor2.x = 650;
	izbor1.w = 50;        izbor2.w = 50;
	izbor1.h = 50;         izbor2.h = 50;



	for (i = 0; i < 10; i++) {
		if (i == 0 || i == 3 || i == 8 || i == 9)
			menus[i] = TTF_RenderText_Solid(font, labels[i], color[0]);
		else
			menus[i] = TTF_RenderText_Solid(font, labels[i], color[1]);
		text[i] = SDL_CreateTextureFromSurface(renderTarget, menus[i]);
	}


	for (i = 0; i < 10; i++) {
		if (i == 0) { pos[i].x = 285;  pos[i].y = 100; }
		if (i == 1 || i == 2) { pos[i].x = 480;  pos[1].y = 170; pos[2].y = 240; }
		if (i == 3) { pos[i].x = 250;  pos[i].y = 350; }
		if (i == 4 || i == 5 || i == 6 || i == 7) { pos[i].x = 480;  pos[4].y = 420;   pos[5].y = 490;   pos[6].y = 560;  pos[7].y = 630; }
		if (i == 8) { pos[i].x = 285;  pos[i].y = 740; }
		if (i == 9) { pos[i].x = 245;  pos[i].y = 850; }
		SDL_QueryTexture(text[i], NULL, NULL, &pos[i].w, &pos[i].h);
	}


	SDL_SetRenderDrawColor(renderTarget, 240, 229, 252, 0xFF);


	while (ex) {
		SDL_RenderClear(renderTarget);
		for (int j = 0; j < 10; j++)
			SDL_RenderCopy(renderTarget, text[j], NULL, &pos[j]);
		if (*velicina == 4) { izbor2.y = 180; SDL_RenderCopy(renderTarget, texture1, NULL, &(izbor1)); }
		if (*velicina == 5) { izbor2.y = 245;  SDL_RenderCopy(renderTarget, texture1, NULL, &(izbor1)); }
		if (*plocica == 256) { izbor1.y = 425;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2)); }
		if (*plocica == 512) { izbor1.y = 500;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2)); }
		if (*plocica == 1024) { izbor1.y = 565;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2)); }
		if (*plocica == 2048) { izbor1.y = 635;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2)); }
		SDL_RenderPresent(renderTarget);


		while (SDL_PollEvent(&e)) {
			switch (e.type)
			{
			case SDL_QUIT:
				ex = 0;
				break;

			case SDL_MOUSEBUTTONDOWN:
				x = e.button.x;
				y = e.button.y;
				for (i = 0; i < 10; i++)
					if ((x >= pos[i].x) && (x <= pos[i].x + pos[i].w) && (y >= pos[i].y) && (y <= pos[i].y + pos[i].h)) {
						if (i == 1) { *velicina = 4; izbor2.y = 200; SDL_RenderCopy(renderTarget, texture1, NULL, &(izbor1));}
						if (i == 2) { *velicina = 5; izbor2.y = 250; SDL_RenderCopy(renderTarget, texture1, NULL, &(izbor1));}
						if (i == 4) { *plocica = 256;  izbor1.y = 300;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2));}
						if (i == 5) { *plocica = 512;  izbor1.y = 350;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2));}
						if (i == 6) { *plocica = 1024;  izbor1.y = 400;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2));}
						if (i == 7) { *plocica = 2048;  izbor1.y = 450;  SDL_RenderCopy(renderTarget, texture2, NULL, &(izbor2));}
						if (i == 8)  napraviBinarnu(*velicina);
						if (i == 9) {
							for (j = 0; j < 10; j++) {
								if (menus[j])SDL_FreeSurface(menus[j]);
								if (text[j])SDL_DestroyTexture(text[j]);
							}
							if (temp) SDL_FreeSurface(temp);
							if (texture1)SDL_DestroyTexture(texture1);
							if (texture2)SDL_DestroyTexture(texture2);
							if (font)TTF_CloseFont(font);
							ex = 0;
							return 1;
						}
					}
				break;
			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE) {
					ex = 0;
				}
			}
		}
	}

	for (j = 0; j < 10; j++) {
		if (menus[j])SDL_FreeSurface(menus[j]);
		if (text[j])SDL_DestroyTexture(text[j]);
	}
	if(temp) SDL_FreeSurface(temp);
	if (texture1)SDL_DestroyTexture(texture1);
	if (texture2)SDL_DestroyTexture(texture2);
	if (font)TTF_CloseFont(font);
	return 1;
}


/* funkcija koja poziva funkicje iz logic.c za pomjeranje kvadratica po tabli; omogucavanje unos imena po zavrsetku igre */
int play(int skor, int *velicina, SDL_Window* window, SDL_Renderer *renderTarget, int vrijednostPlocice) {
	int ex = 1, pravac = 1, kraj = 1, prviPut = 0, x, y, i, j, q = 0,boo=1;
	int **tabla = init(*velicina); 
	char ime[30], a,best[10];
	SDL_Event e;
	SDL_Color color = { 255,255,255 };
	TTF_Font *font = TTF_OpenFont("STENCIL.TTF", 100);
	rang* lista = dohvatiListu(*velicina);
	int bestt;
	bestt = lista[0].brojPoena;
	itoa(bestt, best, 10);

	strcpy(ime, "m");
	//ispisuje se tabla i prva dva elementa ubacena funkcijom init
	tabla = LoadImage(window, renderTarget, tabla, *velicina, skor, vrijednostPlocice, &prviPut,kraj,ime,q,best);

	while (ex) {
		while (SDL_PollEvent(&e)) {
			switch (e.type) {
			case SDL_MOUSEBUTTONDOWN:
				x = e.button.x;
				y = e.button.y;
				if ((x >= 970) && (x <= 1270) && (y >= 100) && (y <= 400)) pravac = pozoviProveru(tabla, *velicina, 3);
				if ((x >= 1050) && (x <= 1200) && (y >= 900) && (y <= 980)) ex = 0;

			case SDL_KEYDOWN:
				switch (e.key.keysym.sym)
				{
				case SDLK_LEFT:  pravac = 1; break;
				case SDLK_RIGHT:  pravac = 3; break;
				case SDLK_UP: pravac = 2; break;
				case SDLK_DOWN: pravac = 4; break;
				case SDLK_a:   ime[q] = 'A'; q++; break;
				case SDLK_b:   ime[q] = 'B'; q++; break;
				case SDLK_c:   ime[q] = 'C'; q++; break;
				case SDLK_d:   ime[q] = 'D'; q++; break;
				case SDLK_e:   ime[q] = 'E'; q++; break;
				case SDLK_f:   ime[q] = 'F'; q++; break;
				case SDLK_g:   ime[q] = 'G'; q++; break;
				case SDLK_h:   ime[q] = 'H'; q++; break;
				case SDLK_i:   ime[q] = 'I'; q++; break;
				case SDLK_j:   ime[q] = 'J'; q++; break;
				case SDLK_k:   ime[q] = 'K'; q++; break;
				case SDLK_l:   ime[q] = 'L'; q++; break;
				case SDLK_m:   ime[q] = 'M'; q++; break;
				case SDLK_n:   ime[q] = 'N'; q++; break;
				case SDLK_o:   ime[q] = 'O'; q++; break;
				case SDLK_p:   ime[q] = 'P'; q++; break;
				case SDLK_q:   ime[q] = 'Q'; q++; break;
				case SDLK_r:   ime[q] = 'R'; q++; break;
				case SDLK_s:   ime[q] = 'S'; q++; break;
				case SDLK_t:   ime[q] = 'T'; q++; break;
				case SDLK_u:   ime[q] = 'U'; q++; break;
				case SDLK_v:   ime[q] = 'V'; q++; break;
				case SDLK_w:   ime[q] = 'W'; q++; break;
				case SDLK_x:   ime[q] = 'X'; q++; break;
				case SDLK_y:   ime[q] = 'Y'; q++; break;
				case SDLK_z:   ime[q] = 'Z'; q++; break;
				case SDLK_1:   ime[q] = '1'; q++; break;
				case SDLK_2:   ime[q] = '2'; q++; break;
				case SDLK_3:   ime[q] = '3'; q++; break;
				case SDLK_4:   ime[q] = '4'; q++; break;
				case SDLK_5:   ime[q] = '5'; q++; break;
				case SDLK_6:   ime[q] = '6'; q++; break;
				case SDLK_7:   ime[q] = '7'; q++; break;
				case SDLK_8:   ime[q] = '8'; q++; break;
				case SDLK_9:   ime[q] = '9'; q++; break;
				case SDLK_0:   ime[q] = '0'; q++; break;
				case SDLK_PERIOD: ime[q] = '.'; q++; break;
				case SDLK_SPACE:  ime[q] = '/'; q++; break;
				}

				if (kraj) {
					potez(tabla, *velicina, pravac, &skor);
					kraj = validnaPoz(tabla, *velicina); //provjerava da li je kraj igre

					//promjene ispisuje na ekranu
					tabla = LoadImage(window, renderTarget, tabla, *velicina, skor, vrijednostPlocice, &prviPut, kraj,ime,q,best);
				}
				
				if (!kraj) {
					if (boo && ((q - 1) < 0)) strcpy(ime, "m");

					if (boo && ((q-1)>=0)) 
						tabla = LoadImage(window, renderTarget, tabla, *velicina, skor, vrijednostPlocice, &prviPut, kraj, ime,q,best);
		
					if ((ime[q-1]=='/') && boo) {
						ime[q-1] = '\0';
						boo = 0;
						upisiUListu(*velicina, skor, ime);
						tabla = LoadImage(window, renderTarget, tabla, *velicina, skor, vrijednostPlocice, &prviPut, kraj, ime, q, best);
					}
				}

			} 
		} 
	} 	

	if (font) TTF_CloseFont(font);
	return skor;
}

/* sve promjene nastale u tabli, skor, hint, povratak na meni se prikazuju na ekranu */
int** LoadImage(SDL_Window* window, SDL_Renderer* renderTarget, int **tabla, int velicina, int skor, int vrijednostPlocice,int *prviPut,int kraj,char znak[30],int br,char best[10]) {
	int quit = 0,boo=0, i, j, dimezija = velicina*velicina, x = -1,q=0,ex=1;
	char *img, skorText[10], temp[2];
	SDL_Event ev;
	SDL_Texture *texture1 = NULL, *texture2 = NULL, *text = NULL, *text1 = NULL, *ispis[30] = { NULL };
	SDL_Rect pozadina, hint, textRect, textRect1,ispisPoz[30];
	SDL_Rect pozicija[25], pozPodloge;
	SDL_Texture *texture[25] = {NULL}, *podloga = IMG_LoadTexture(renderTarget, "podloga.bmp");
	SDL_Surface *textSurface = NULL, *textSurface1 = NULL, *surface[30] = { NULL };
	SDL_Color color = { 255,255,255 };
	TTF_Font *font = TTF_OpenFont("arialbd.TTF", 40);
	TTF_Font *font1 = TTF_OpenFont("STENCIL.TTF", 110), *font2 = TTF_OpenFont("STENCIL.TTF", 83);
	SDL_Rect textRectGO, pozIme, pozBest;
	SDL_Texture *textt = NULL, *textIme = NULL, *textBest = NULL;
	SDL_Surface *textSurfaceGO = NULL, *surfaceIme = NULL, *surfaceBest = NULL;
	textRectGO.x = 195; pozIme.x = 142;	textRectGO.w = pozIme.w = 50;
	textRectGO.y = 315; pozIme.y = 440;	textRectGO.h = pozIme.h = 50;
	
	textRect.h = 50;    textRect1.h = 400;      ispisPoz[br].x = 250;	pozPodloge.x = 135;     pozBest.x = 760;
	textRect.w = 50;    textRect1.w = 400;		ispisPoz[br].y = 565;	pozPodloge.y = 300;     pozBest.y = 60;
	textRect.x = 550;	textRect1.x = 185;								pozPodloge.w = 720;      pozBest.w = 50;
	textRect.y = 60;	textRect1.y = 500;								pozPodloge.h = 380;      pozBest.h = 50;
	

	for (i = 0; i < 25; i++)
		texture[i] = NULL;

	if (velicina == 5) {
		textRect.h = 50;      pozBest.x = 642;			pozPodloge.x = 55;			textRectGO.x = 100;		pozIme.x = 65;      textRect.x = 200; 
		textRect.w = 50;      pozBest.y = 100;			pozPodloge.y = 400;			textRectGO.y = 415;     pozIme.y = 540;		ispisPoz[br].x = 225;
		textRect.x = 415;	  pozBest.h = 50;			pozPodloge.w = 720;			textRectGO.w = pozIme.w = 50;				ispisPoz[br].y = 650;
		textRect.y = 100;	   pozBest.w = 50;			pozPodloge.h = 380;			textRectGO.h = pozIme.h = 50;				textRect1.x = 95;
	}

	texture1 = IMG_LoadTexture(renderTarget, "hint.bmp");
	if(dimezija==16)
	   texture2 = IMG_LoadTexture(renderTarget, "praznatabela.bmp");
	else
	texture2 = IMG_LoadTexture(renderTarget, "6x6.bmp");
	//sluzi za ispis best skora
	surfaceBest = TTF_RenderText_Solid(font, best, color);
	textBest = SDL_CreateTextureFromSurface(renderTarget, surfaceBest);
	SDL_QueryTexture(textBest, NULL, NULL, &pozBest.w, &pozBest.h);
	//sluzi za ispis skora
	itoa(skor, skorText,10);
	textSurface = TTF_RenderText_Solid(font, skorText, color);
	text= SDL_CreateTextureFromSurface(renderTarget, textSurface);
	SDL_QueryTexture(text, NULL, NULL, &textRect.w, &textRect.h);
	//za ispis Cestitimo! kada se postigne 2048
	textSurface1 = TTF_RenderText_Solid(font1, "Cestitamo!", color);
	text1 = SDL_CreateTextureFromSurface(renderTarget, textSurface1);
	SDL_QueryTexture(text1, NULL, NULL, &textRect1.w, &textRect1.h);
	//sluzi za GAME OVER
	textSurfaceGO = TTF_RenderText_Solid(font1, "KRAJ IGRE!", color);
	textt = SDL_CreateTextureFromSurface(renderTarget, textSurfaceGO);
	SDL_QueryTexture(textt, NULL, NULL, &textRectGO.w, &textRectGO.h);
	//sluzi za ispis Unesite ime:
	surfaceIme = TTF_RenderText_Solid(font1, "Unesite ime: ", color);
	textIme = SDL_CreateTextureFromSurface(renderTarget, surfaceIme);
	SDL_QueryTexture(textIme, NULL, NULL, &pozIme.w, &pozIme.h);
	//ispis IMENA
	znak[br + 1] = '\0';
	surface[br] = TTF_RenderText_Solid(font2, znak, color);
	ispis[br] = SDL_CreateTextureFromSurface(renderTarget,surface[br]);
	SDL_QueryTexture(ispis[br], NULL, NULL, &ispisPoz[br].w, &ispisPoz[br].h);


	for (int i = 0; i < velicina; i++) {
		for (int j = 0; j < velicina; j++) {
			x++;
			if (tabla[i][j] == 2) img = "2.bmp"; else if (tabla[i][j] == 8) img = "8.bmp"; else if (tabla[i][j] == 4) img = "4.bmp"; else if (tabla[i][j] == 16) img = "16.bmp";
			else if (tabla[i][j] == 32) img = "32.bmp";  else if (tabla[i][j] == 64) img = "64.bmp";  else if (tabla[i][j] == 128) img = "128.bmp";
			else if (tabla[i][j] == 256) img = "256.bmp";  else if (tabla[i][j] == 512) img = "512.bmp";  else if (tabla[i][j] == 1024) img = "1024.bmp";
			else if (tabla[i][j] == 2048) img = "2048.bmp";  else if (tabla[i][j] == 4096) img = "4096.bmp";  else img = "8192.bmp";


			if (tabla[i][j])	texture[x] = IMG_LoadTexture(renderTarget, img);
			if (velicina == 4)
			    { pozicija[x].w = 183;  pozicija[x].h = 183;}
			else
			  { pozicija[x].w = 138;  pozicija[x].h = 138;  }

		
			if (tabla[i][j] == vrijednostPlocice) { boo = 1;
			if (*prviPut == 0)
				*prviPut = 1;
			else
				*prviPut = 2;
			}


			if (velicina == 4) {
				if (j == 0) pozicija[x].x = 93;    if (j == 1) pozicija[x].x = 299;   if (j == 2) pozicija[x].x = 503;   if (j == 3) pozicija[x].x = 707;
				if (i == 0) { pozicija[x].y = 185;  if (j == 0) pozicija[x].x = 94; }
				if (i == 1)  pozicija[x].y = 385;
				if (i == 2)  pozicija[x].y = 587;
				if (i == 3)  pozicija[x].y = 790;
			}
			else
			{
				if (j == 0) pozicija[x].x = 39;    if (j == 1) pozicija[x].x = 194;   if (j == 2) pozicija[x].x = 348;  
				if (j == 3) pozicija[x].x = 500;   if (j == 4) pozicija[x].x = 657;    

				if (i == 0) { pozicija[x].y = 208;  if (j == 0) pozicija[x].x = 40; if (j == 2) pozicija[x].x = 343; if (j == 4) pozicija[x].x = 650;}
				if (i == 1) {	if (j == 4) pozicija[x].x = 650; if (j == 2)  pozicija[x].x = 344;  pozicija[x].y = 361;  }
				if (i == 2) { pozicija[x].y = 516; if (j == 2) pozicija[x].x = 344; if (j == 3)  pozicija[x].x = 495; if (j == 4) pozicija[x].x = 652; }
				if (i == 3) { pozicija[x].y = 669; if (j == 1) pozicija[x].x = 190; if (j == 2)  pozicija[x].x = 344; if (j == 4)  pozicija[x].x = 652;}
				if (i == 4) { pozicija[x].y = 824;  if (j == 2)  pozicija[x].x = 344; if (j == 4)  pozicija[x].x = 653;
				}
			}

		}
		
	}
	
	pozadina.x = 0;
	pozadina.y = 0;
	pozadina.w = 1450;
	pozadina.h = 1020;

	hint.x = 970;
	hint.y = 100;
	hint.w = 300;
	hint.h = 300;

	x = -1;


	SDL_RenderClear(renderTarget);
	SDL_RenderCopy(renderTarget, texture2, NULL, &(pozadina));
	SDL_RenderCopy(renderTarget, texture1, NULL, &(hint));
	for (int i = 0; i < velicina; i++) {
		for (int j = 0; j < velicina; j++) {
			x++;
			if (tabla[i][j])
				SDL_RenderCopy(renderTarget, texture[x], NULL, &(pozicija[x]));
		}
	}
	SDL_RenderCopy(renderTarget, textBest, NULL, &pozBest);
	SDL_RenderCopy(renderTarget, text, NULL, &textRect);
	if ((boo) && (*prviPut == 1)) {
		pozPodloge.h = 180; pozPodloge.y = 450;
		SDL_RenderCopy(renderTarget, podloga, NULL, &pozPodloge);
		SDL_RenderCopy(renderTarget, text1, NULL, &(textRect1));
	}
	if (!kraj) { 
		SDL_RenderCopy(renderTarget, podloga, NULL, &pozPodloge);
		SDL_RenderCopy(renderTarget, textt, NULL, &textRectGO); 
		SDL_RenderCopy(renderTarget, textIme, NULL, &pozIme);	
		if ((znak[br]!='m')&&(znak[br-1]!='/')) SDL_RenderCopy(renderTarget,ispis[br], NULL, &(ispisPoz[br]));
	}
	SDL_RenderPresent(renderTarget);
	boo = 0;
	
	if (podloga) SDL_DestroyTexture(podloga);
	if (texture1)SDL_DestroyTexture(texture1);
	if (texture2)SDL_DestroyTexture(texture2);
	if (text1)SDL_DestroyTexture(text1);
	if (text)SDL_DestroyTexture(text);
	if (textSurface)SDL_FreeSurface(textSurface);
	if (textSurface1)SDL_FreeSurface(textSurface1);
	if (font)TTF_CloseFont(font);
	if (font1)TTF_CloseFont(font1);
	if (font2)TTF_CloseFont(font2);
	if (textSurfaceGO) SDL_FreeSurface(textSurfaceGO);
	if (textt) SDL_DestroyTexture(textt);
	if (surfaceIme) SDL_FreeSurface(surfaceIme);
	if (textIme) SDL_DestroyTexture(textIme);
	if (textBest)SDL_DestroyTexture(textBest);
	if (surfaceBest) SDL_FreeSurface(surfaceBest);
	for (i = 0; i < 30; i++) {
		if (surface[i]) SDL_FreeSurface(surface[i]);
		if (ispis[i]) SDL_DestroyTexture(ispis[i]);
	}
	for (i = 0; i < 25; i++) 
	  	if(texture[i]) SDL_DestroyTexture(texture[i]);
	

	return tabla;
}


/*prikazuje text koji predstavlja pomoc(informacije o igri) */
void info(SDL_Window* window, SDL_Renderer *renderTarget) {
	int ex = 1, x, y;
	SDL_Color color = { 150,0,0 };
	SDL_Event e;
	TTF_Font *font = TTF_OpenFont("STENCIL.TTF", 40);
	SDL_Surface *textSurface = NULL;
	SDL_Texture *text = NULL;
	SDL_Rect textRect;
	SDL_Texture *texture = IMG_LoadTexture(renderTarget, "oigri.bmp");
	SDL_Rect poz;
	poz.x = 0;
	poz.y = 0;
	poz.w = 1250;
	poz.h = 1000;

	textSurface = TTF_RenderText_Solid(font, "Povratak na meni", color);
	text = SDL_CreateTextureFromSurface(renderTarget, textSurface);

	textRect.x = 850;  textRect.y = 950;
	SDL_QueryTexture(text, NULL, NULL, &textRect.w, &textRect.h);

	while (ex)
	{
		SDL_RenderClear(renderTarget);
		SDL_RenderCopy(renderTarget, texture, NULL, &(poz));
		SDL_RenderCopy(renderTarget, text, NULL, &textRect);
		SDL_RenderPresent(renderTarget);
		while (SDL_PollEvent(&e) != 0)
		{
			switch (e.type)
			{
			case SDL_MOUSEBUTTONDOWN:
				x = e.button.x;
				y = e.button.y;
				if ((x >= textRect.x) && (x <= textRect.x + textRect.w) && (y >= textRect.y) && (y <= textRect.y + textRect.h)) {
					if (text)SDL_DestroyTexture(text);
					if (textSurface)SDL_FreeSurface(textSurface);
					if (texture)SDL_DestroyTexture(texture);
					if (font)TTF_CloseFont(font);
					return;
				}
				break;
			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE)
					ex = 0;
				if (text)SDL_DestroyTexture(text);
				if (textSurface)SDL_FreeSurface(textSurface);
				if (texture)SDL_DestroyTexture(texture);
				if (font)TTF_CloseFont(font);
				return 0;
			}
		}
	}


	if (text)SDL_DestroyTexture(text);
	if (textSurface)SDL_FreeSurface(textSurface);
	if (texture)SDL_DestroyTexture(texture);
	if (font)TTF_CloseFont(font);
}


/* prikazuje imena autora na ekranu  */
void autori(SDL_Window* window, SDL_Renderer *renderTarget, int x) {
	int ex = 1, i,z,y,j;
	SDL_Color color = { 150,0,0 };
	SDL_Event e;
	TTF_Font *font = TTF_OpenFont("arialbd.TTF", 70), *font1 = TTF_OpenFont("arialbd.TTF", 63);
	SDL_Surface *textSurface[4] = {NULL};
	SDL_Texture *text[4] = {NULL};
	SDL_Rect textRect[4];
	SDL_Texture *texture1 = IMG_LoadTexture(renderTarget, "etf.bmp"), *texture2 = IMG_LoadTexture(renderTarget, "povratak.bmp");
	SDL_Rect poz, poz1;
	poz.x = 880;   poz1.x = 40;
	poz.y = 650;	poz1.y = 940;
	poz.w = 270;	poz1.w = 130;
	poz.h = 300;    poz1.h = 80;

	//ISPISIVANJE TEKSTA 
	textSurface[0] = TTF_RenderText_Solid(font, "Filip Tanic - HINT ALGORITAM", color);
	textSurface[1] = TTF_RenderText_Solid(font, "Matija Bojovic - LOGIKA IGRE", color);
	textSurface[2] = TTF_RenderText_Solid(font, "Maja Skoko - GRAFIKA", color);
	textSurface[3] = TTF_RenderText_Solid(font1, "JUN 2017", color);
	text[0] = SDL_CreateTextureFromSurface(renderTarget, textSurface[0]);
	text[1] = SDL_CreateTextureFromSurface(renderTarget, textSurface[1]);
	text[2] = SDL_CreateTextureFromSurface(renderTarget, textSurface[2]);
	text[3] = SDL_CreateTextureFromSurface(renderTarget, textSurface[3]);

	textRect[0].x = 15;  textRect[1].x = 15;   textRect[2].x = 15;   textRect[3].x = 540;
	textRect[0].y = 20;  textRect[1].y = 120;  textRect[2].y = 220;  textRect[3].y = 875;  
	SDL_QueryTexture(text[0], NULL, NULL, &textRect[0].w, &textRect[0].h);
	SDL_QueryTexture(text[1], NULL, NULL, &textRect[1].w, &textRect[1].h);
	SDL_QueryTexture(text[2], NULL, NULL, &textRect[2].w, &textRect[2].h);
	SDL_QueryTexture(text[3], NULL, NULL, &textRect[3].w, &textRect[3].h);

	SDL_SetRenderDrawColor(renderTarget, 232, 232, 232, 0xFF);

	while (ex)
	{
		SDL_RenderClear(renderTarget);
		SDL_RenderCopy(renderTarget, text[0], NULL, &textRect[0]);
		SDL_RenderCopy(renderTarget, text[1], NULL, &textRect[1]);
		SDL_RenderCopy(renderTarget, text[2], NULL, &textRect[2]);
		SDL_RenderCopy(renderTarget, text[3], NULL, &textRect[3]);
		SDL_RenderCopy(renderTarget, texture1, NULL, &(poz));
		SDL_RenderCopy(renderTarget, texture2, NULL, &(poz1));
		SDL_RenderPresent(renderTarget);
		while (SDL_PollEvent(&e) != 0)
		{
			switch (e.type)
			{
			case SDL_QUIT:
				ex = 0;
				break;
			case SDL_MOUSEBUTTONDOWN:
				z = e.button.x;
				y = e.button.y;
				if ((z >= poz1.x) && (z <= poz1.x + poz1.w) && (y >= poz1.y) && (y <= poz1.y + poz1.h)) {
					ex = 0;
				}
			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE)
					ex = 0;
			}
		}
	}

	//OSLOBADJANJE MEMORIJE
	for (i = 0; i < 4; i++) {
		if(textSurface[i])SDL_FreeSurface(textSurface[i]);
		textSurface[i] = NULL;
		if(text[i])SDL_DestroyTexture(text[i]);
		text[i] = NULL;
	}
	if(texture1)SDL_DestroyTexture(texture1);
	if(texture2)SDL_DestroyTexture(texture2);
	if(font)TTF_CloseFont(font);
	if (font1)TTF_CloseFont(font1);
}